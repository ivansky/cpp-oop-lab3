# Laboratory work №3

## Necessary tools
- [**CMake**](https://cmake.org/download/)
- Compiler *(one of them)*
    * [**GCC 6 +**](https://gcc.gnu.org/install/binaries.html)
    * [**CLang 3.5 +**](http://releases.llvm.org/download.html)
    * [**MSVC 19 +**](http://landinghub.visualstudio.com/visual-cpp-build-tools)

## Build
Windows Cmd

`$ ./build.bat`

Unix / MacOS terminal

`$ chmod +x ./build.sh && ./build.sh`


## Run
Windows Cmd

`$ ./main.exe`

Unix / MacOS terminal

`$ ./main`

## Task

Implement FIFO algorithm in C++.

## Author

© Ivan Martianov
