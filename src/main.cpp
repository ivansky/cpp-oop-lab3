#include <iostream>
#include <string>
#include "FIFO.h"

using namespace std;

int main(int argc, const char * argv[]) {
    FIFO newTail;
    string label;
    Node newNode;
    auto getNode = new Node;
    char sel;

    while (true)
    {
        cout << "Choose one of the following operations:\n\n";
        cout << "a - Add an element to your TAIL;\n";
        cout << "b - Print your queue;\n";
        cout << "c - Get the last element;\n";
        cout << "d - Remove the last element.\n";
        cout << "q - Quit.\n\n>> ";

        cin >> sel;

        switch(sel)
        {
            case 'a':
                cout << "Enter a label: ";
                cin >> label;
                newNode.label = label;
                newTail.enqueue(newNode);
                cout << endl << endl;
                break;
            case 'b':
                newTail.print();
                cout << endl << endl;
                break;
            case 'c':
                newTail.getNode(getNode);
                cout << getNode -> label << endl << endl;
                break;
            case 'd':
                newTail.dequeue(getNode);
                cout << getNode -> label << endl << endl;
                break;
            case 'q':
                return 0;
            default: break;
        }
    }
}