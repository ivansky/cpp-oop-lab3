#ifndef OOPLAB2_FIFO_H
#define OOPLAB2_FIFO_H

#include <iostream>
#include <string>

using namespace std;

struct Node
{
    string label;
    Node * next;
};

class FIFO
{
private:
    Node * head;
    Node * tail;

public:
    FIFO();
    bool isEmpty();
    void enqueue(Node new_node);
    void dequeue(Node * & getNode);
    void getNode(Node * & getNode);
    void print();
};


#endif //OOPLAB2_FIFO_H
